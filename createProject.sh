#!/bin/bash

if [ $# -ne 2 ]
then
    echo "Uso: . createProject nombreProyecto nombreAplicacion"
else
    django-admin.py startproject $1
    cd $1
    python manage.py startapp $2
    echo "Abre ahora create.txt"
    echo ""
    
    #Seteamos las vistas por default de la aplicación
    cp ../aux/app_views $2/views.py
    sed -i "s/\$2/$2/g" $2/views.py
    
    #Actualizamos url del proyecto
    cp ../aux/project_urls $1/urls.py
    sed -i "s/\$2/$2/g" $1/urls.py
    
    #Actualizamos url de la aplicación
    cp ../aux/app_urls $2/urls.py
    sed -i "s/\$2/$2/g" $2/urls.py
    
    #Modificamos el wsgi
    cp ../aux/wsgi $1/wsgi.py
    sed -i "s/\$1/$1/g" $1/wsgi.py
    
    #Escribimos los models
    cp ../aux/app_models $2/models.py
    
    #Escribimos Procfile para poder usarlo en heroku
    echo "web: gunicorn $1.wsgi --log-file -" > Procfile
    
    #Creamos la carpeta de las templates y los estáticos
    mkdir -p templates/$2
    mkdir -p static/img
    mkdir static/css
    mkdir static/js
fi

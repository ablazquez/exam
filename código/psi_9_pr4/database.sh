#!/bin/bash

dropdb -U alumnodb -h localhost $1
export PGPASSWORD=alumnodb
export DATABASE_URL='postgres://alumnodb:alumnodb@localhost:5432/'$1
createdb -U alumnodb -h localhost $1
python manage.py makemigrations
python manage.py migrate
if [ $# -eq 2 ]
then
echo "from django.contrib.auth.models import User; User.objects.create_superuser('alumnodb', 'alumno@uam.com', 'alumnodb')" | python manage.py shell &> /dev/null
echo "Created superuser alumnodb"
fi

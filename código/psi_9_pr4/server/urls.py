from django.conf.urls import url
from server import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^nologged/', views.nologged, name='nologged'),
    url(r'^index/', views.index, name='index'),
    url(r'^register_user/', views.register, name='register_user'),
    url(r'^login_user/', views.login_user, name='login_user'),
    url(r'^logout_user/', views.logout_user, name='logout_user'),
    url(r'^board_hound/', views.board_hound, name='board_hound'),
    url(r'^board_fox/', views.board_fox, name='board_fox'),
    url(r'^join_game/', views.join_game, name='join_game'),
    url(r'^wait_fox/', views.wait_fox, name='wait_fox'),
    url(r'^status_board/', views.status_board, name='status_board'),
    url(r'^status_turn/', views.status_turn, name='status_turn'),
    url(r'^move/', views.move, name='move')
]

urlpatterns += staticfiles_urlpatterns()

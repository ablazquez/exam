from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from server.models import Counter, Game, Move
from server.forms import UserForm, moveForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

def nologged(request):
    """
    Function that renders the logout page
    author: Arturo Blazquez Perez
    """
    contextDict = {}
    
    if 'username' in request.session:
        contextDict['username']=request.session['username']
    
    return render(request, 'server/nologged.html', contextDict)

def index(request):
    """
    Function that renders the main page
    author: Gonzalo Ruiz-Oriol
    """
    contextDict = {}
    
    if 'username' in request.session:
        contextDict['username']=request.session['username']
    
    return render(request, 'server/base.html', contextDict)

@login_required
def board_hound(request):
    """
    Function that renders the counter page
    author: Arturo Blazquez Perez
    """
    #We get the user from the database
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

    u = User.objects.filter(username=username)
    if u.exists():
        user=u[0]
    else:
        return HttpResponse("Error: couldn't find your user id")

    #We create a new game with our user and save it to the database
    game=Game(houndUser=user, foxUser=None)
    game.save()

    #We save into a session variable the gameid and your role in the game and we render the game template
    request.session['gameID']=game.id
    request.session['amIhound']=True

    return render(request, 'server/board_hound.html', {'username': username} )

@login_required
def board_fox(request):
    """
    Function that renders the counter page
    author: Arturo Blazquez Perez
    """
    #We get the user from the database
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

    u = User.objects.filter(username=username)
    if u.exists():
        user=u[0]
    else:
        return HttpResponse("Error: couldn't find your user id")

    #TODO: We search for a game and join
    request.session['amIhound']=False

    return render(request, 'server/board_fox.html', {'username': username} )

def board(request):
    """
    Function that renders the board
    author: Arturo Blazquez Perez
    """
    #We get the data from the form and we get the game from the database
    if 'gameID' in request.session:
        gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")

    g = Game.objects.filter(id=gameid)
    if g.exists():
        game=g[0]
    else:
        return HttpResponse("Error: couldn't find your game id")
    
    ret = "<table>"
    
    for i in range(0, 8):
        ret+="<tr>"
        for j in range(0, 8):
            if game.hound1 == i*8+j or game.hound2 == i*8+j or game.hound3 == i*8+j or game.hound4 == i*8+j:
                ret+="<td><img class='contenido' src='/static/img/hound.png' alt='hound' /></td>"
            elif game.fox == i*8+j:
                ret+="<td><img class='contenido' src='/static/img/fox.png' alt='fox' /></td>"
            else:
                ret+="<td></td>"
        ret+="</tr>"
    
    ret+="</table>"

    return HttpResponse(ret)

def register(request):
    """
    Function that renders the register page
    author: Gonzalo Ruiz-Oriol
    """
    contextDict = {}
    
    if 'username' in request.session:
        contextDict['username']=request.session['username']

    #If we have submitted the form, we process the user. If not we show the form
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        #If the user is valid, we add it to the database
        if user_form.is_valid():
            user = user_form.save()

            user.set_password(user.password)
            user.save()

            return HttpResponseRedirect('/server/login_user')
        else:
            print user_form.errors
    else:
        contextDict['user_form'] = UserForm()

    # Render the template depending on the context.
    return render(request, 'server/register.html', contextDict )

def login_user(request):
    """
    Function that renders the login page
    author: Arturo Blazquez Perez
    """
    
    #If we haven't logged in yet, we show a form. Else we welcome the user
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')
        choose=request.POST.get('choose')

        user = authenticate(username=username, password=password)

        #If we have introduced a correct username/password, we welcome the user
        if user:
            if user.is_active:
                login(request, user)
                request.session['username'] = username
                if(choose=='hound'):
                    return HttpResponseRedirect('/server/board_hound')
                else:
                    return HttpResponseRedirect('/server/board_fox')
            else:
                return HttpResponse("Your account is disabled")

        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details")

    else:
        return render(request, 'server/login.html', {} )

@login_required
def logout_user(request):
    """
    Function that renders the logout page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We logout and show the user a goodbye page
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")
    logout(request)

    return render(request, 'server/logout.html', {'usernameout':username} )

@login_required
def create_game(request):
    """
    Function that renders the create game page
    author: Arturo Blazquez Perez
    """
    
    #We get the user from the database
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

    u = User.objects.filter(username=username)
    if u.exists():
        user=u[0]
    else:
        return HttpResponse("Error: couldn't find your user id")

    #We create a new game with our user and save it to the database
    game=Game(houndUser=user, foxUser=None)
    game.save()

    #We save into a session variable the gameid and your role in the game and we render the game template
    request.session['gameID']=game.id
    request.session['amIhound']=True

    return render(request, 'server/game.html', {'user': user, 'game': game} )

@login_required
def clean_orphan_games(request):
    """
    Function that renders the clear page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We get from the database every game with no foxUser and delete it. Then we render the clean template
    rows_count = Game.objects.filter(foxUser__isnull=True).delete()

    return render(request, 'server/clean.html', {'rows_count': rows_count[0]} )

@login_required
def join_game(request):
    """
    Function that renders the join page
    author: Arturo Blazquez Perez
    """
    
    #We search in the database for available games
    games = Game.objects.filter(foxUser__isnull=True)
    if games.exists():
        #We get the last created game
        game=games[len(games)-1]

        #We get the user from the database, and add it to the game
        if 'username' in request.session:
            username=request.session['username']
        else:
            return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

        u = User.objects.filter(username=username)
        if u.exists():
            user=u[0]
        else:
            return HttpResponse("Error: couldn't find your user id")

        game.foxUser=user
        game.save()

        #We save into a session variable the gameid and your role in the game
        request.session['gameID']=game.id
        request.session['amIhound']=False
        
        return status_board(request)
    else:
        return HttpResponse("Waiting for a game")

@login_required
def wait_fox(request):
    """
    Function that renders the join page
    author: Arturo Blazquez Perez
    """
    
    if 'gameID' in request.session:
            gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id")
    
    g = Game.objects.filter(id=gameid, foxUser__isnull=True)
    if g.exists():
        return HttpResponse("Waiting for fox")
    else:
        return status_board(request)

def hound_move(game, origin, target):
    """
    Function that moves a hound
    author: Gonzalo Ruiz-Oriol
    """
    
    borderLeft=[0,16,32,48]
    borderRigth=[15,31,47]
    occupied=[game.hound1,game.hound2,game.hound3,game.hound4,game.fox]
    valid=False

    #We check that we don't move to an occupied square
    if target in occupied:
        return HttpResponse("There is a Hound in the target")

    #We check that the orgin is inside the board and then check if the target is valid(hounds can only move forward)
    if origin<0 or origin>54:
        return HttpResponse("Cannot create move")
    elif origin in borderLeft:
        if target==origin+9:
            valid=True
        elif target==origin-7:
            return HttpResponse("Cannot create move")
    elif origin in borderRigth:
        if target==origin+7:
            valid=True
        elif target==origin-9:
            return HttpResponse("Cannot create move")
    elif target==origin+7 or target==origin+9:
        valid=True
    else:
        return HttpResponse("Cannot create move")
    
    #With this logic you cannot move to a black square so we don't need to check that the target is a black one
    
    if game.houndTurn==True and valid==True:
        #We check which hound you want to move, we move it and update the database
        if game.hound1==origin:
            game.hound1=target
        elif game.hound2==origin:
            game.hound2=target
        elif game.hound3==origin:
            game.hound3=target
        elif game.hound4==origin:
            game.hound4=target
        else:
            return HttpResponse("Cannot create move")
        
        game.houndTurn=False;
        game.save()

        #We create the move and save it to the database
        move=Move(origin=origin, target=target, game=game)
        move.save()

        return HttpResponse("Move done")
    else:
        return HttpResponse("Cannot create move")

def fox_move(game, target):
    """
    Function that moves a hound
    author: Arturo Blazquez Perez
    """
    
    
    borderLeft=[16,32,48]
    borderRigth=[15,31,47]
    bottom=[57,59,61]
    occupied=[game.hound1,game.hound2,game.hound3,game.hound4,game.fox]


    valid=False
    origin=game.fox

    #We check that the orgin is inside the board and then check if the target is valid(fox can move forward and backwards)
    if origin<8 or origin>63:
        valid=False
    elif origin==63:
        if target==54:
            valid=True
    elif origin in bottom:
        if target==origin-9 or target==origin-7:
            valid=True
    elif origin in borderLeft:
        if target==origin+9 or target==origin-7:
            valid=True
    elif origin in borderRigth:
        if target==origin+7 or target==origin-9:
            valid=True
    elif target==origin+7 or target==origin+9 or target==origin-7 or target==origin-9:
        valid=True
    else:
        return HttpResponse("Cannot create move")

    #We check that we don't move to an occupied square
    if target in occupied:
        return HttpResponse("There is a Hound in the target")


    if game.houndTurn==False and valid==True:
        #We update the fox position and update the database
        game.houndTurn=True;
        game.fox=target
        game.save()

        #We create the move and save it to the database
        move=Move(origin=origin, target=target, game=game)
        move.save()

        return HttpResponse("Move done")
    else:
        return HttpResponse("Cannot create move")

@login_required
def move(request):
    """
    Function that renders the move page
    author: Gonzalo Ruiz-Oriol
    """
    
    #If we have submitted the form, we process it. If not we show the form
    if request.method=='POST':
        #We get the data from the form and we get the game from the database
        origin=int(request.POST.get('origin'))
        target=int(request.POST.get('target'))
        if 'gameID' in request.session:
            gameid=request.session['gameID']
        else:
            return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
        hound=request.session['amIhound']


        g = Game.objects.filter(id=gameid)
        if g.exists():
            game=g[0]
        else:
            return HttpResponse("Error: couldn't find your game id")

        #We check that you are moving your pieces and do the move
        if game.houndTurn and hound==True:
            return hound_move(game, origin, target)
        elif not game.houndTurn and hound==False:
            return fox_move(game, target)
        else:
            return HttpResponse("Cannot create move")
    else:
        return HttpResponse("Error, you must POST")


@login_required
def status_turn(request):
    """
    Function that renders the turn page
    author: Arturo Blazquez Perez
    """
    
    #We get your game from the database
    if 'gameID' in request.session:
        gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
    if 'amIhound' in request.session:
        hound=request.session['amIhound']
    else:
        return HttpResponse("Error: couldn't find hound Turn <br /> <a href='/server/index'>Index</a>")

    g = Game.objects.filter(id=gameid)
    if g.exists():
        game=g[0]
    else:
        return HttpResponse("Error: couldn't find your game id")

    #If it is your turn we print True. If not False
    if game.houndTurn == hound:
        return HttpResponse("True")
    else:
        return HttpResponse("False")

@login_required
def status_board(request):
    """
    Function that renders the board page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We get your game from the database
    if 'gameID' in request.session:
        gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
    if 'username' in request.session:
        user=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your user id <br /> <a href='/server/index'>Index</a>")
    if 'amIhound' in request.session:
        hound=request.session['amIhound']
    else:
        return HttpResponse("Error: couldn't find hound Turn <br /> <a href='/server/index'>Index</a>")

    g = Game.objects.filter(id=gameid)
    if g.exists():
        game=g[0]
    else:
        return HttpResponse("Error: couldn't find hound game")

    #We create the board empty
    Board = [0 for i in xrange(64)]

    #We insert the hounds on the empty board
    Board[game.hound1] = 1
    Board[game.hound2] = 1
    Board[game.hound3] = 1
    Board[game.hound4] = 1

    #We insert the fox on the empty board and render it
    Board[game.fox] = -1

    return render(request, 'server/game_ajax.html', {'board': Board, 'myTurn': game.houndTurn == hound})

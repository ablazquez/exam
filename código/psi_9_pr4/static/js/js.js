var myRefresh;
var lastUsed = -1;
var myturn;
var iamhound;

function refresh_hound(){
    $.ajax({
        url: '/server/wait_fox/',
        success: function (data) {
            if(data=="Waiting for fox"){
                setTimeout(refresh_hound, 200);
            } else {
                $("#content").html(data);
                adjust();
            }
        }
    })
}

function refresh(){
    if(myturn){
        clearTimeout(myRefresh);
    } else {
        $.ajax({
            url: '/server/status_board/',
            success: function (data) {
                $("#content").html(data);
                myRefresh = setTimeout(refresh, 200);
                adjust();
            }
        })
    }
}

function refresh_fox(){
    $.ajax({
        url: '/server/join_game/',
        success: function (data) {
            if(data=="Waiting for a game"){
                setTimeout(refresh_fox, 200);
            } else {
                $("#content").html(data);
                adjust();
                refresh();
            }
        }
    })
}

function refresh_turn(){
    $.ajax({
        url: '/server/status_turn/',
        success: function (data) {
            if(data=="True")
                setTimeout(setturn, 400);
            else
                setTimeout(unsetturn, 400);
        }
    })
}

function setturn(){
    myturn=true;
    refresh_turn();
}

function unsetturn(){
    myturn=false;
    refresh_turn();
}

function adjust(){
    if(document.getElementById("content").offsetHeight<document.getElementById("content").offsetWidth){
        document.getElementsByTagName("table")[0].style.width = document.getElementById("content").offsetHeight-5;
    } else {
        document.getElementsByTagName("table")[0].style.width = document.getElementById("content").offsetWidth-5;
    }
}

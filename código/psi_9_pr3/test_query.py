import os,django
os.environ.setdefault('DJANGO_SETTINGS_MODULE' , 'houndFox.settings')
django.setup()
from server.models import Game, Move
from django.contrib.auth.models import User

id=10
username='u10'
password='p10'
u = User.objects.filter(id=id)
if u.exists():
    u10=u[0]
else:
    u10=User(id=id,username=username, password=password)
    u10.save()


id=11
username='u11'
password='p11'
u = User.objects.filter(id=id)
if u.exists():
    u11=u[0]
else:
    u11=User(id=id,username=username, password=password)
    u11.save()


game=Game(houndUser=u10, foxUser=None)
game.save()
g = Game.objects.filter(foxUser__isnull=True)

game.foxUser = u11
game.save()

move=Move(origin=2, target=11, game=game)
move.save()
game.hound2 = 11
game.houndTurn = False
game.save()

move2=Move(origin=59, target=52, game=game)
move2.save()
game.fox = 52
game.houndTurn = True
game.save()

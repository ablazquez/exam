#!/bin/bash

dropdb -U alumnodb -h localhost $1
export PGPASSWORD=alumnodb
export DATABASE_URL='postgres://alumnodb:alumnodb@localhost:5432/'$1
createdb -U alumnodb -h localhost $1
python manage.py makemigrations
python manage.py migrate
if [ $2 -eq 1 ]
then
python manage.py createsuperuser
fi

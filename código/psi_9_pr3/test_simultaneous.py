# Uncomment if you want to run tests in transaction mode with a final rollback
#from django.test import TestCase
#uncomment this if you want to keep data after running tests
from unittest import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test import Client
from server.models import Game, Move, Counter

#python ./manage.py test rango.tests.UserAuthentiHoundionTests --keepdb
#class UserAuthentiHoundionTests(TestCase):

usernameHound1 = 'houndUser1'
passwdHound1 = 'houndPasswd1'
usernameHound2 = 'houndUser2'
passwdHound2 = 'houndPasswd3'
usernameHound3 = 'houndUser3'
passwdHound3 = 'houndPasswd3'
usernameFox1 = 'foxUser1'
passwdFox1 = 'foxPasswd1'
usernameFox2 = 'foxUser2'
passwdFox2 = 'foxPasswd2'
usernameFox3 = 'foxUser3'
passwdFox3 = 'foxPasswd3'
DEBUG = False

class ServerTests(TestCase):
    def setUp(self):
        self.clientHound1 = Client()
        self.clientHound2 = Client()
        self.clientHound3 = Client()
        self.clientFox1 = Client()
        self.clientFox2 = Client()
        self.clientFox3 = Client()

    def creatUser(self, userName, userPassword):
        try:
            user = User.objects.get(username=userName)
        except User.DoesNotExist:
            user = User(username=userName, password=userPassword)
            user.set_password(user.password)
            user.save()
        return user.id

    def login(self, userName, userPassword, client):
        response = client.get(reverse('login_user'))
        self.assertIn(b'Login', response.content)
        loginDict={}
        loginDict["username"]=userName
        loginDict["password"]=userPassword
        response = client.post(reverse('login_user'), loginDict, follow=True)
        return response


    def test_simultaneous(self):
        #Create hounds and foxes users
        userHoundId1 = self.creatUser(usernameHound1, passwdHound1)
        userHoundId2 = self.creatUser(usernameHound2, passwdHound2)
        userHoundId3 = self.creatUser(usernameHound3, passwdHound3)
        userFoxID1 = self.creatUser(usernameFox1, passwdFox1)
        userFoxID2 = self.creatUser(usernameFox2, passwdFox2)
        userFoxID3 = self.creatUser(usernameFox3, passwdFox3)
        #delete all orphan pages
        response = self.clientHound1.get(reverse('clean_orphan_games'))
        #login
        response = self.login(usernameHound1, passwdHound1,self.clientHound1)
        response = self.login(usernameHound2, passwdHound2,self.clientHound2)
        response = self.login(usernameHound3, passwdHound3,self.clientHound3)
        response = self.login(usernameFox1, passwdFox1,self.clientFox1)
        response = self.login(usernameFox2, passwdFox2,self.clientFox2)
        response = self.login(usernameFox3, passwdFox3,self.clientFox3)
        #create games
        response = self.clientHound1.get(reverse('create_game'))#follow redirection
        self.assertEqual(response.status_code, 200)
        #join game
        response = self.clientFox1.get(reverse('join_game'))
        
        response = self.clientHound2.get(reverse('create_game'))#follow redirection
        self.assertEqual(response.status_code, 200)
        #join game
        response = self.clientFox2.get(reverse('join_game'))
        
        response = self.clientHound3.get(reverse('create_game'))#follow redirection
        self.assertEqual(response.status_code, 200)
        #join game
        response = self.clientFox3.get(reverse('join_game'))
        
        #Make moves on different games
        moveDict={}
        moveDict['origin']=0
        moveDict['target']=9
        response = self.clientHound1.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'houndUser = %s (%d)'%(usernameHound1, userHoundId1),response.content)
        
        moveDict['origin']=59
        moveDict['target']=50
        response = self.clientFox1.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'FoxUser = %s (%d)'%(usernameFox1, userFoxID1),response.content)
        
        
        moveDict['origin']=4
        moveDict['target']=11
        response = self.clientHound2.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'houndUser = %s (%d)'%(usernameHound2, userHoundId2),response.content)
        
        moveDict['origin']=59
        moveDict['target']=52
        response = self.clientFox2.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'FoxUser = %s (%d)'%(usernameFox2, userFoxID2),response.content)
        
        
        moveDict['origin']=6
        moveDict['target']=15
        response = self.clientHound3.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'houndUser = %s (%d)'%(usernameHound3, userHoundId3),response.content)
        
        moveDict['origin']=59
        moveDict['target']=50
        response = self.clientFox3.post(reverse('move'), moveDict)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'FoxUser = %s (%d)'%(usernameFox3, userFoxID3),response.content)
        
        response = self.clientHound1.get(reverse('status_board'))
        self.assertIn(b"""<td id=id_9 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_2 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_4 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_6 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_50 style='width: 20px'>\n              &#9920;""",response.content)
        response = self.clientHound2.get(reverse('status_board'))
        self.assertIn(b"""<td id=id_0 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_2 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_11 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_6 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_52 style='width: 20px'>\n              &#9920;""",response.content)
        response = self.clientHound3.get(reverse('status_board'))
        self.assertIn(b"""<td id=id_0 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_2 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_4 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_15 style='width: 20px'>\n              &#9922;""",response.content)
        self.assertIn(b"""<td id=id_50 style='width: 20px'>\n              &#9920;""",response.content)

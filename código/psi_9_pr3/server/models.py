from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

class Game(models.Model):
    houndUser = models.ForeignKey(User, related_name='game_houndUser')
    foxUser = models.ForeignKey(User, null=True, blank=True)
    hound1 = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound2 = models.IntegerField(default=2, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound3 = models.IntegerField(default=4, validators=[MaxValueValidator(63), MinValueValidator(0)])
    hound4 = models.IntegerField(default=6, validators=[MaxValueValidator(63), MinValueValidator(0)])
    fox = models.IntegerField(default=59, validators=[MaxValueValidator(63), MinValueValidator(0)])
    houndTurn = models.BooleanField(default=True)

    def __unicode__(self):      #For Python 2, use __str__ on Python 3
        if(self.foxUser):
            return "(H)" + self.houndUser.username + " vs (F)" + self.foxUser.username + " houndTurn: " + str(self.houndTurn)
        else:
            return "(H)" + self.houndUser.username + " vs (F)" + "(null)" + " houndTurn: " + str(self.houndTurn)


class Move(models.Model):
    origin = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])
    target = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)])
    game = models.ForeignKey(Game)
    
    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        return str(self.origin) + "->" + str(self.target) +  " [" + str(self.game) + "]"


class Counter(models.Model):
    counterglobal = models.IntegerField(default=0)

    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        return str(self.counterglobal)

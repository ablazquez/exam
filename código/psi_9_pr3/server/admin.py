from django.contrib import admin
from server.models import Game, Move
from django.contrib.auth.models import User

admin.site.register(Game)
admin.site.register(Move)

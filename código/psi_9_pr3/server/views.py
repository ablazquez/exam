from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from server.models import Counter, Game, Move
from server.forms import UserForm, moveForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

def nologged(request):
    """
    Function that renders the logout page
    author: Arturo Blazquez Perez
    """
    return render(request, 'server/nologged.html')

def index(request):
    """
    Function that renders the main page
    author: Gonzalo Ruiz-Oriol
    """
    return render(request, 'server/index.html')

def counter(request):
    """
    Function that renders the counter page
    author: Arturo Blazquez Perez
    """
    contextDict = {}

    #We increment by one the session variable. If it's not yet created, we initialize it to one
    if 'counterSes' in request.session:
        request.session['counterSes'] += 1
    else:
        request.session['counterSes'] = 1

    #We take the counter from the data base
    counterList = Counter.objects.all()
    if counterList.exists():
        counterObj = counterList[0]
    else:
        counterObj = Counter()

    #We increment the counter on the database and render the page
    counterObj.counterglobal += 1
    counterObj.save()

    contextDict['counterSes']= request.session['counterSes']
    contextDict['counterGlobal']=counterObj.counterglobal
    return render(request, 'server/counter.html', contextDict)

def register(request):
    """
    Function that renders the register page
    author: Gonzalo Ruiz-Oriol
    """
    registered = False

    #If we have submitted the form, we process the user. If not we show the form
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)

        #If the user is valid, we add it to the database
        if user_form.is_valid():
            user = user_form.save()

            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print user_form.errors
    else:
        user_form = UserForm()

    # Render the template depending on the context.
    return render(request, 'server/register.html', {'user_form': user_form, 'registered': registered} )

def login_user(request):
    """
    Function that renders the login page
    author: Arturo Blazquez Perez
    """
    
    #If we haven't logged in yet, we show a form. Else we welcome the user
    if request.method=='POST':
        username=request.POST.get('username')
        password=request.POST.get('password')

        user = authenticate(username=username, password=password)

        #If we have introduced a correct username/password, we welcome the user
        if user:
            if user.is_active:
                login(request, user)
                request.session['username'] = username
                return HttpResponseRedirect('/server/login_user')
            else:
                return HttpResponse("Your account is disabled")

        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details")

    else:
        return render(request, 'server/login.html', {} )

@login_required
def logout_user(request):
    """
    Function that renders the logout page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We logout and show the user a goodbye page
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")
    logout(request)

    return render(request, 'server/logout.html', {'username':username} )

@login_required
def create_game(request):
    """
    Function that renders the create game page
    author: Arturo Blazquez Perez
    """
    
    #We get the user from the database
    if 'username' in request.session:
        username=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

    u = User.objects.filter(username=username)
    if u.exists():
        user=u[0]
    else:
        return HttpResponse("Error: couldn't find your user id")

    #We create a new game with our user and save it to the database
    game=Game(houndUser=user, foxUser=None)
    game.save()

    #We save into a session variable the gameid and your role in the game and we render the game template
    request.session['gameID']=game.id
    request.session['amIhound']=True

    return render(request, 'server/game.html', {'user': user, 'game': game} )

@login_required
def clean_orphan_games(request):
    """
    Function that renders the clear page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We get from the database every game with no foxUser and delete it. Then we render the clean template
    rows_count = Game.objects.filter(foxUser__isnull=True).delete()

    return render(request, 'server/clean.html', {'rows_count': rows_count[0]} )

@login_required
def join_game(request):
    """
    Function that renders the join page
    author: Arturo Blazquez Perez
    """
    
    #We search in the database for available games
    games = Game.objects.filter(foxUser__isnull=True)
    if games.exists():
        #We get the last created game
        thereIsGame = True
        game=games[len(games)-1]

        #We get the user from the database, and add it to the game
        if 'username' in request.session:
            username=request.session['username']
        else:
            return HttpResponse("Error: couldn't find your username <br /> <a href='/server/index'>Index</a>")

        u = User.objects.filter(username=username)
        if u.exists():
            user=u[0]
        else:
            return HttpResponse("Error: couldn't find your user id")

        game.foxUser=user
        game.save()

        #We save into a session variable the gameid and your role in the game
        request.session['gameID']=game.id
        request.session['amIhound']=False
    else:
        thereIsGame = False
        game = -1

    return render(request, 'server/join.html', {'thereIsGame': thereIsGame, 'game': game, 'error': "No games available :("} )

def hound_move(game, origin, target):
    """
    Function that moves a hound
    author: Gonzalo Ruiz-Oriol
    """
    
    borderLeft=[0,16,32,48]
    borderRigth=[15,31,47]
    occupied=[game.hound1,game.hound2,game.hound3,game.hound4,game.fox]
    valid=False

    #We check that we don't move to an occupied square
    if target in occupied:
        return {'game': game, 'error': "cannot be a Hound in the target", 'moveDone': False}

    #We check that the orgin is inside the board and then check if the target is valid(hounds can only move forward)
    if origin<0 or origin>54:
        return {'game': game, 'error': "Cannot create a move", 'moveDone': False}
    elif origin in borderLeft:
        if target==origin+9:
            valid=True
        elif target==origin-7:
            return {'game': game, 'error': " you must move to a contiguous diagonal place", 'moveDone': False}
    elif origin in borderRigth:
        if target==origin+7:
            valid=True
        elif target==origin-9:
            return {'game': game, 'error': " you must move to a contiguous diagonal place", 'moveDone': False}
    elif target==origin+7 or target==origin+9:
        valid=True
    else:
        return {'game': game, 'error': " you must move to a contiguous diagonal place", 'moveDone': False}
    
    #With this logic you cannot move to a black square so we don't need to check that the target is a black one
    
    if game.houndTurn==True and valid==True:
        #We check which hound you want to move, we move it and update the database
        if game.hound1==origin:
            game.hound1=target
        elif game.hound2==origin:
            game.hound2=target
        elif game.hound3==origin:
            game.hound3=target
        elif game.hound4==origin:
            game.hound4=target
        else:
            return {'game': game, 'error': "Cannot create a move", 'moveDone': False}
        
        game.houndTurn=False;
        game.save()

        #We create the move and save it to the database
        move=Move(origin=origin, target=target, game=game)
        move.save()

        return {'game': game, 'move': move, 'moveDone': True}
    else:
        return {'game': game, 'error': "Cannot create a move", 'moveDone': False}

def fox_move(game, target):
    """
    Function that moves a hound
    author: Arturo Blazquez Perez
    """
    
    
    borderLeft=[16,32,48]
    borderRigth=[15,31,47]
    bottom=[57,59,61]
    occupied=[game.hound1,game.hound2,game.hound3,game.hound4,game.fox]


    valid=False
    origin=game.fox

    #We check that the orgin is inside the board and then check if the target is valid(fox can move forward and backwards)
    if origin<8 or origin>63:
        valid=False
    elif origin==63:
        if target==54:
            valid=True
    elif origin in bottom:
        if target==origin-9 or target==origin-7:
            valid=True
    elif origin in borderLeft:
        if target==origin+9 or target==origin-7:
            valid=True
    elif origin in borderRigth:
        if target==origin+7 or target==origin-9:
            valid=True
    elif target==origin+7 or target==origin+9 or target==origin-7 or target==origin-9:
        valid=True
    else:
        return {'game': game, 'error': "you must move to a contiguous diagonal place", 'moveDone': False}

    #We check that we don't move to an occupied square
    if target in occupied:
        return {'game': game, 'error': "cannot be a Hound in the target", 'moveDone': False}


    if game.houndTurn==False and valid==True:
        #We update the fox position and update the database
        game.houndTurn=True;
        game.fox=target
        game.save()

        #We create the move and save it to the database
        move=Move(origin=origin, target=target, game=game)
        move.save()

        return {'game': game, 'move': move, 'moveDone': True}
    else:
        return {'game': game, 'error': "Cannot create a move", 'moveDone': False} #TODO: devolver un error no generico

@login_required
def move(request):
    """
    Function that renders the move page
    author: Gonzalo Ruiz-Oriol
    """
    
    #If we have submitted the form, we process it. If not we show the form
    if request.method=='POST':
        #We get the data from the form and we get the game from the database
        origin=int(request.POST.get('origin'))
        target=int(request.POST.get('target'))
        if 'gameID' in request.session:
            gameid=request.session['gameID']
        else:
            return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
        hound=request.session['amIhound']


        g = Game.objects.filter(id=gameid)
        if g.exists():
            game=g[0]
        else:
            return HttpResponse("Error: couldn't find your game id")

        #We check that you are moving your pieces and do the move
        if game.houndTurn and hound==True:
            return render(request, 'server/move.html', hound_move(game, origin, target))
        elif not game.houndTurn and hound==False:
            return render(request, 'server/move.html', fox_move(game, target))
        else:
            return render(request,  'server/move.html',{'error':"Cannot create a move"})
    else:
        return render(request, 'server/move.html', {'move_form': moveForm()})


@login_required
def status_turn(request):
    """
    Function that renders the turn page
    author: Arturo Blazquez Perez
    """
    
    #We get your game from the database
    if 'gameID' in request.session:
        gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
    if 'amIhound' in request.session:
        hound=request.session['amIhound']
    else:
        return HttpResponse("Error: couldn't find hound Turn <br /> <a href='/server/index'>Index</a>")

    g = Game.objects.filter(id=gameid)
    if g.exists():
        game=g[0]
    else:
        return HttpResponse("Error: couldn't find your game id")

    #If it is your turn we print True. If not False
    if game.houndTurn == hound:
        return render(request, 'server/turn.html', {'myTurn': True})
    else:
        return render(request,'server/turn.html', {'myTurn': False})

@login_required
def status_board(request):
    """
    Function that renders the board page
    author: Gonzalo Ruiz-Oriol
    """
    
    #We get your game from the database
    if 'gameID' in request.session:
        gameid=request.session['gameID']
    else:
        return HttpResponse("Error: couldn't find your game id <br /> <a href='/server/index'>Index</a>")
    if 'username' in request.session:
        user=request.session['username']
    else:
        return HttpResponse("Error: couldn't find your user id <br /> <a href='/server/index'>Index</a>")

    g = Game.objects.filter(id=gameid)
    if g.exists():
        game=g[0]
    else:
        return {}

    #We create the board empty
    Board = [0 for i in xrange(64)]

    #We insert the hounds on the empty board
    Board[game.hound1] = 1
    Board[game.hound2] = 1
    Board[game.hound3] = 1
    Board[game.hound4] = 1

    #We insert the fox on the empty board and render it
    Board[game.fox] = -1

    return render(request, 'server/board.html', {'board': Board})
